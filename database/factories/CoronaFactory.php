<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Corona;
use Faker\Generator as Faker;

$factory->define(Corona::class, function (Faker $faker) {

    $country = $faker->randomElement(['indonesia', 'malaysia', 'singapore', 'philippine', 'timor leste']);
    $symptoms = $faker->randomElement(['bokek', 'ngantuk', 'demam', 'capek', 'pusing', 'batuk']);    

    return [
        'country_name' => $country,
        'symptoms' => $symptoms,
        'cases' => rand(800, 2500),
        'user_id' => rand(54,62)
    ];
});
