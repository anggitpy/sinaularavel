@extends('layout')

@section('content')

<style>
  .uper {
    margin-top: 40px;
  }
</style>

<div class="uper">
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
    @endif
</div>

<a href="{{ route('coronas.create') }}" class="btn btn-primary mb-4">Add New Case</a>
<h4>{{ URL::asset('assets/images/favicon.ico') }}</h4>

<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Country Name</th>
            <th>Symptoms</th>
            <th>Cases</th>
            <th>User</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($coronacases as $case)
        <tr>
            <td>{{ $case->id }}</td>
            <td>{{ $case->country_name }}</td>
            <td>{{ $case->symptoms }}</td>
            <td>{{ $case->cases }}</td>
            <td>{{ $case->user->name }}</td>
            <td>
                <a href="{{ route('coronas.edit', $case->id) }}" class="btn btn-primary btn-sm">Edit</a>
            </td>
            <td>
                <form action="{{ route('coronas.destroy', $case->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection